<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>HTML5 Scheduler</title>
        <link rel="stylesheet" href="css/compre_color.css">
        <script src="js/daypilot/daypilot-all.min.js"></script>
	   <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="header">
        </div>

        <div class="main">
          <div id="dp"></div>
        </div>
            <script type="text/javascript">
                var dp = new DayPilot.Scheduler("dp");

                dp.treeEnabled = true;
                dp.theme = "compre_color";

                dp.cellWidthSpec = "Auto";
                dp.eventHeight = 40;
                dp.cellWidth = 100;
                dp.cellDuration = 30;
                dp.locale = "es-mx";

                dp.timeHeaders = [
                  { groupBy: "Month", format: "dd MMMM yyyy" },
                  { groupBy:  "Hour", format: "h:mm" }
                ];

                dp.eventDeleteHandling = "Actualizar";

                dp.onEventMoved = function (args) {
                    $.post("backend_move.php",
                    {
                        id: args.e.id(),
                        newStart: args.newStart.toString(),
                        newEnd: args.newEnd.toString(),
                        newResource: args.newResource
                    },
                    function() {
                        dp.message("Movidio");
                    });
                };

                dp.onEventResized = function (args) {
                    $.post("backend_resize.php",
                    {
                        id: args.e.id(),
                        newStart: args.newStart.toString(),
                        newEnd: args.newEnd.toString()
                    },
                    function() {
                        dp.message("Redimencionado");
                    });
                };

                dp.onEventDeleted = function(args) {
                    $.post("backend_delete.php",
                    {
                        id: args.e.id()
                    },
                    function() {
                        dp.message("Eliminado");
                    });
                };

                dp.onTimeRangeSelected = function (args) {
                    var url = "new.php?start=" +args. start + "&end=" + args.end + "&resource=" + args.resource;

                    var modal = new DayPilot.Modal();
                    modal.onClosed = function(args) {
                        dp.clearSelection();
                        var data = args.result;
                        if(data && data.result==="OK") { loadEvents(); dp.message(data.message); }
                        if(data && data.result==="ERROR") { loadEvents(); dp.message("Error: " + data.message); }
                    };
                    modal.showUrl(url);
                };

                dp.onEventClick = function(args) {
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        // reload all events
                        var data = this.result;
                        if (data && data.result === "OK") {
                            loadEvents();
                        }
                    };
                    modal.showUrl("edit.php?id=" + args.e.id());
                };

                dp.init();

                loadResources();
                loadEvents();

                function loadEvents() {
                    var start = dp.startDate;
                    var end = dp.startDate.addDays(dp.days);

                    $.post("backend_events.php",
                        {
                            start: start.toString(),
                            end: end.toString()
                        },
                        function(data) {
                            dp.events.list = data;
                            dp.update();
                        }
                    );
                }

                function loadResources() {
                    $.post("backend_resources.php", function(data) {
                        dp.resources = data;
                        dp.update();
                    });
                }

            </script>
    </body>
</html>
