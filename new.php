<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Event</title>
    	<link type="text/css" rel="stylesheet" href="css/dialog.css" />
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
            $start = $_GET['start']; // TODO parse and format
            $end = $_GET['end']; // TODO parse and format
            $resource = $_GET['resource'];   // TODO validate
        ?>
        <form id="f" action="backend_create.php" style="padding:20px;">
            <h1>New Event</h1>
            <div>Name: </div>
            <div><input type="text" id="name" name="name" value="" /></div>
            <div>Start:</div>
            <div><input type="text" id="start" name="start" value="<?php echo $start ?>" /></div>
            <div>End:</div>
            <div><input type="text" id="end" name="end" value="<?php echo $end ?>" /></div>
            <div>Resource:</div>
            <div><input type="text" id="resource" name="resource" value="<?php echo $resource ?>" /></div>
            <div class="space"><input type="submit" value="Save" /> <a href="javascript:close();">Cancel</a></div>
        </form>

        <script type="text/javascript">
        function close(result) {
            DayPilot.Modal.close(result);
        }

        $("#f").submit(function () {
            var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
                close(eval(result));
            });
            return false;
        });

        $(document).ready(function () {
            $("#name").focus();
        });

        </script>
    </body>
</html>
